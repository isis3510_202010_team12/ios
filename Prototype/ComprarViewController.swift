//
//  ComprarViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/26/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit

class ComprarViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabelView: UILabel!
    @IBOutlet var ratingLabelView: UILabel!
    @IBOutlet var descriptionLabelView: UILabel!
    @IBOutlet var dateLabelView: UILabel!
    @IBOutlet var cantidadLabelView: UILabel!
    @IBOutlet var precioLabelView: UILabel!
    
    
    var name = ""
    var rating = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabelView.text = name
        ratingLabelView.text = rating

        // Do any additional setup after loading the view.
    }
    

 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
