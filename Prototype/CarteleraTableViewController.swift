//
//  CarteleraTableViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/21/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class CarteleraTableViewController: UIViewControllerReach, UITableViewDelegate, UITableViewDataSource {
 
    
    
    @IBOutlet var obrasTableView: UITableView!
    var obras = NSMutableArray()
    var id = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    
        let user = Auth.auth().currentUser
        let _name = UserDefaults.standard.object(forKey: "name") as! String?
        let _date = UserDefaults.standard.object(forKey: "birthdate") as! String?
        let _username = UserDefaults.standard.object(forKey: "username") as! String?
        if _username != nil && _username==user?.email{
            print("Encontré user defaults ")
            print(_name, _date, _username)
        }
        else{
            if(checkReachable2())
            {
                if (user != nil){
                    let reference = Database.database().reference().child("users").queryOrdered(byChild: "username")
                    reference.queryEqual(toValue: user?.email).observeSingleEvent(of: .childAdded) { (snapshot) in
                        let dictionary = snapshot.value as! [String : Any]
                        let name = dictionary["name"] as? String
                        let date = dictionary["birthdate"] as? String
                        let username = dictionary["username"] as? String
                        
                        UserDefaults.standard.set(username, forKey: "username")
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(date, forKey: "birthdate")
                               
                        print("Guardé userDefaults")
                    }
                }
            }
        }
        
        self.obrasTableView.delegate = self
        self.obrasTableView.dataSource = self
        loadData()


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    func loadData(){
        Database.database().reference().child("obras").observeSingleEvent(of: .value, with: { (snapchot) in

            if  let obrasDictionary = snapchot.value {
                for obra in obrasDictionary as![String: AnyObject]{
                
                    let llave = obra.key
                    let valor = obra.value
                    let title = valor["title"]
                    let description = valor["description"]
                    let about = valor["about"]
                    let rating = valor["rating"]
                    let theatre = valor["theatre"]
                    let scene = valor["scene"]
                         
                    
                   // Insertando a user Default con imagen
                    var image = Data()
                    let imageName = valor["image"] as? String
                    let imageRef = Storage.storage().reference().child("images/\(imageName!)")
                    imageRef.getData(maxSize: 25 * 1024 * 1024, completion: { (data, error) -> Void in
                        if error == nil{
                                   //successful
                                image = data!
                                let obraDic = ["title":title, "description":description, "about":about, "rating":rating, "theatre":theatre, "scene":scene, "image":image] as [String : Any]
                               
                                UserDefaults.standard.set(obraDic, forKey: llave)

                               } else{
                                   //error
                                print("Error downloading image: \(error?.localizedDescription)")
                               let obraDic = ["title":title, "description":description, "about":about, "rating":rating, "theatre":theatre, "scene":scene] as [String : Any]
                               UserDefaults.standard.set(obraDic, forKey: llave)
                               }
                        })
                    /*
                    
                    //Insertando a UserDefault sin imagen
                    let obraDic = ["title":title, "description":description, "about":about, "rating":rating, "theatre":theatre, "scene":scene] as [String : Any]
                    UserDefaults.standard.set(obraDic, forKey: llave)

                    */
 
                    //Mostrando de BD
                    // self.obras.add(obra.value)
                    
                    //Mostrando de UserDefault
                    self.obras.add(llave)

                }
                self.obrasTableView.reloadData()
                
            }
        })
        
       // let _name = UserDefaults.standard.object(forKey: "name") as! String?
        
      //  print("Antes de recuperar")
      //  let obrasRecuperadas = UserDefaults.standard.object(forKey: "cartelera") as Any
      //  print(obrasRecuperadas)
        
      //  print("Despues de recuperar")

        
    }
 
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.obras.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ObraTableViewCell
        
        let llave = self.obras[indexPath.row] as! String
        let obra = UserDefaults.standard.object(forKey: llave) as! Dictionary<String ,AnyObject>
        cell.descriptionLabelView.text = obra["description"] as? String
        cell.titleLabelView.text = obra["title"] as? String
        let num = obra["rating"] as? Double
        cell.ratingLabelView.text = "\(num!)"
        
        
         let imageData = obra["image"] as! Data
         let image = UIImage(data: imageData)
         cell.obraImageView.image = image

 
        //Mostrando informacion de BD
      /*
        let obra = self.obras[indexPath.row] as! [String: AnyObject]
        cell.titleLabelView.text = obra["title"] as? String
        cell.descriptionLabelView.text = obra["description"] as? String
        let num = obra["rating"] as? Double
        cell.ratingLabelView.text = "\(num!)"
       if  let imageName = obra["image"] as? String{
        let imageRef = Storage.storage().reference().child("images/\(imageName)")
        imageRef.getData(maxSize: 25 * 1024 * 1024, completion: { (data, error) -> Void in
            if error == nil{
                //successful
                print("Imagen encontrada")
                let image = UIImage(data: data!)
                cell.obraImageView.image = image
            } else{
                //error
                print("Error downloading image: \(error?.localizedDescription)")
            }
            
        })
    }
 */
        cell.titleLabelView.alpha = 0
        cell.descriptionLabelView.alpha = 0
        cell.obraImageView.alpha = 0
        
        UIView.animate(withDuration: 0.4, animations: {
            
            cell.titleLabelView.alpha = 1
            cell.descriptionLabelView.alpha = 1
            cell.obraImageView.alpha = 1
            
        })
        // Configure the cell...

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ObraDetailViewController") as? ObraDetailViewController
        
        let llave = self.obras[indexPath.row] as! String
               let obra = UserDefaults.standard.object(forKey: llave) as! Dictionary<String ,AnyObject>
               vc?.name = obra["title"] as! String
               let num = obra["rating"] as? Double
               vc?.rating = "\(num!)"
               vc?.resumen = obra["about"] as! String
               vc?.theatre = obra["theatre"] as! String
               vc?.escena = obra["scene"] as! String
               
                let imageData = obra["image"] as! Data
                let image = UIImage(data: imageData)
                vc?.image = image as! UIImage

        
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
