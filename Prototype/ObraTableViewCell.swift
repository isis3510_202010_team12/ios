//
//  ObraTableViewCell.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/21/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit

class ObraTableViewCell: UITableViewCell {

    @IBOutlet var titleLabelView: UILabel!
    @IBOutlet var obraImageView: UIImageView!
    @IBOutlet var descriptionLabelView: UILabel!
    @IBOutlet var ratingLabelView: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
