//
//  QRScannerViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/25/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet var messageLabel: UILabel!
    var captureSession:AVCaptureSession!
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
         
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("No se pudo acceder a la camara")
            return
        }
         
        do {
           
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            
       
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
                 qrCodeFrameView = UIView()
                   
                  if let qrCodeFrameView = qrCodeFrameView {
                      qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                      qrCodeFrameView.layer.borderWidth = 2
                      view.addSubview(qrCodeFrameView)
                      view.bringSubviewToFront(qrCodeFrameView)
                  }
            
            
        } catch {
            print(error)
            return
        }

        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)

        captureSession.startRunning()
        view.bringSubviewToFront(messageLabel)

        // Do any additional setup after loading the view.
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
           if metadataObjects.count == 0 {
               qrCodeFrameView?.frame = CGRect.zero
               messageLabel.text = "No QR code is detected"
               return
           }
           
           let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
           
           if metadataObj.type == AVMetadataObject.ObjectType.qr {
               let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
               qrCodeFrameView?.frame = barCodeObject!.bounds
               
               if metadataObj.stringValue != nil {
                   messageLabel.text = metadataObj.stringValue
               }
           }
           }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
