//
//  FirstViewController.swift
//  Prototype
//
//  Created by Luis Alfonso Ruiz on 27/02/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import Firebase

class FirstViewController: UIViewControllerReach {

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = Auth.auth().currentUser
        let _name = UserDefaults.standard.object(forKey: "name") as! String?
        let _date = UserDefaults.standard.object(forKey: "birthdate") as! String?
        let _username = UserDefaults.standard.object(forKey: "username") as! String?
        if _username != nil && _username==user?.email{
            print("Encontré user defaults ")
            print(_name, _date, _username)
        }
        else{
            if(checkReachable2())
            {
                if (user != nil){
                    let reference = Database.database().reference().child("users").queryOrdered(byChild: "username")
                    reference.queryEqual(toValue: user?.email).observeSingleEvent(of: .childAdded) { (snapshot) in
                        let dictionary = snapshot.value as! [String : Any]
                        let name = dictionary["name"] as? String
                        let date = dictionary["birthdate"] as? String
                        let username = dictionary["username"] as? String
                        
                        UserDefaults.standard.set(username, forKey: "username")
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(date, forKey: "birthdate")
                        print("Guardé userDefaults")
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
}

