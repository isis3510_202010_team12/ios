//
//  CalificacionViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 5/2/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import Firebase

class CalificacionViewController: UIViewController {


    @IBOutlet var textField: UITextView!
    @IBOutlet var numberLabel: UILabel!

    @IBAction func ratingSlider(_ sender: UISlider) {
        
        numberLabel.text = String(format: "%.1f", sender.value)
    }
    
    @IBAction func sendTapped(_ sender: UIButton) {
        
        if let rating = numberLabel.text{
                 if let comentario = textField.text{
                         let userObject: Dictionary<String,Any> = [
                             "rating" : rating,
                             "comentario" : comentario
                             
                         ]
                         Database.database().reference().child("calificaciones").childByAutoId().setValue(userObject)
                         print("Calificacion agregada")
                     
                 }
             }
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destViewController: ProfileViewController = segue.destination as! ProfileViewController
        
        destViewController.ratingNuevo = numberLabel.text as! String
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
