//
//  ObraDetailViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/22/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import Firebase

class ObraDetailViewController: UIViewController {

    @IBOutlet var videoImageView: UIImageView!
    @IBOutlet var nameLabelView: UILabel!
    @IBOutlet var resumenLabelView: UILabel!
    @IBOutlet var escenaLabelView: UILabel!
    @IBOutlet var ratingLabelView: UILabel!
    @IBOutlet var theatreLabelView: UILabel!
    
    var image = UIImage()
    var name = ""
    var resumen = ""
    var escena = ""
    var theatre = ""
    var rating = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabelView.text = name
        resumenLabelView.text = resumen
        ratingLabelView.text = rating
        escenaLabelView.text = escena
        theatreLabelView.text = theatre
        videoImageView.image = image
        

        // Do any additional setup after loading the view.
      /*  print("Antes de recuperar")
              let obrasRecuperadas = UserDefaults.standard.object(forKey: "cartelera") as! AnyObject
              print(obrasRecuperadas)
              print("Despues de recuperar")
        
        let _name = UserDefaults.standard.object(forKey: "name") as! String?
*/
    }
    

    @IBAction func comprarTapped(_ sender: Any) {
        
        
        let user = Auth.auth().currentUser
        

                if (user != nil){
                    let reference = Database.database().reference().child("users").queryOrdered(byChild: "username")
                    reference.queryEqual(toValue: user?.email).observeSingleEvent(of: .childAdded) { (snapshot) in
                        
                        let key = snapshot.key
                        let dictionary = snapshot.value as! [String : Any]

                     //   let obras = dictionary["obras"]
                     //     if obras == nil{

                            let randomName = self.randomStringWithLength(length: 5)
                            let userObject = [randomName: self.name]
                            Database.database().reference().child("users").child(key).child("obras").childByAutoId().setValue(userObject)
                            print("Obras agregadas")
                            //No tiene obras asociadas
                            
                     //   }else{
                        /*
                            let randomName = self.randomStringWithLength(length: 5)
                            let userObject = [randomName: self.name]
                            Database.database().reference().child("users").child(key).child("obras").childByAutoId().setValue(userObject)

                            print("ya tengo obras")
                         */
                            
                            //Tiene obras asociadsas, se agrega nueva
                            
                      //  }
    
            
                    }
                
            }
        

       // self.name = nameLabelView.text!
       // performSegue(withIdentifier: "Comprar", sender: self)
    }
    
    func randomStringWithLength(length: Int) -> NSString{
        let characters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString: NSMutableString = NSMutableString(capacity: length)
        for _ in 0..<length {
            let len = UInt32(characters.length)
            let rand = arc4random_uniform(len)
            randomString.appendFormat("%C", characters.character(at: Int(rand)))
        }
        return randomString
    }
    
   //   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    var vc = segue.destination as! ComprarViewController
    //    vc.name = self.name
   // }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
