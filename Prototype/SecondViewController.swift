//
//  SecondViewController.swift
//  Prototype
//
//  Created by Luis Alfonso Ruiz on 27/02/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    struct Teatro:Codable {
      var nombre: String
      var lattitude: CLLocationDegrees
      var longtitude: CLLocationDegrees
    }
    
    //Aquí lo hacemos manualmente, pero posteriormente vendrá esta info de firebase
    let teatrosInicial = [Teatro(nombre: "Teatro Mayor Julio Mario Santo Domingo", lattitude: 4.757014, longtitude: -74.062606),
    Teatro(nombre: "Teatro Belarte", lattitude: 4.727057, longtitude: -74.024421),
    Teatro(nombre: "Teatro Montessori", lattitude: 4.716648, longtitude: -74.077795),
    Teatro(nombre: "Teatro Servitá", lattitude: 4.741598, longtitude: -74.022909),
    Teatro(nombre: "Teatro Colsubsidio", lattitude: 4.623141, longtitude: -74.077962),
    Teatro(nombre: "William Shakespeare Auditorium", lattitude: 4.735878, longtitude: -74.043401),
    Teatro(nombre: "Teatro Patria", lattitude: 4.685110, longtitude: -74.036180),
    Teatro(nombre: "Teatro ABC", lattitude: 4.689912, longtitude: -74.049050),
    Teatro(nombre: "Teatro Nacional Calle 71", lattitude: 4.655697, longtitude: -74.058592),
    Teatro(nombre: "Teatro Astor Plaza", lattitude: 4.653363, longtitude: -74.061915)]
    
    let initialLocation = CLLocation(latitude: 4.633697, longitude: -74.082926)

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        var teatrosGuardados:[Teatro] = []
        checkLocationServices()
        let defaults = UserDefaults.standard
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(teatrosInicial){
            defaults.set(encoded, forKey:"teatros")
            //let jsonString = String(data: encoded, encoding: .utf8)
            //print("JSON String : " + jsonString!)
        }
        
        if let teatrosD = defaults.object(forKey:"teatros") as? Data{
            let decoder = JSONDecoder()
            if let loadedTeatros = try? decoder.decode([Teatro].self, from: teatrosD){
                teatrosGuardados = loadedTeatros
                //print(teatrosGuardados.count)
            }
        }
        
        mapView.delegate = self
        locationManager.delegate = self
        
        
        // Adiciona los teatros al mapa
        fetchTeatrosOnMap(teatrosGuardados)
    }
    
    func checkLocationServices() {
      if CLLocationManager.locationServicesEnabled() {
        checkLocationAuthorization()
      } else {
        // Show alert letting the user know they have to turn this on.
      }
    }
    func checkLocationAuthorization() {
        
      switch CLLocationManager.authorizationStatus() {
      case .authorizedWhenInUse:
        mapView.showsUserLocation = true
        mapView.centerToLocation(initialLocation)
       case .denied: // Show alert telling users how to turn on permissions
        //mapView.centerToLocation(initialLocation)
        break
      case .notDetermined:
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
      case .restricted: // Show an alert letting them know what’s up
        //mapView.centerToLocation(initialLocation)
       break
      case .authorizedAlways:
       break
      }
    }
    
    func fetchTeatrosOnMap(_ teatros: [Teatro]){
        for teatro in teatros{
            let annotation = MKPointAnnotation()
            annotation.title = teatro.nombre
            annotation.coordinate = CLLocationCoordinate2D(latitude: teatro.lattitude, longitude: teatro.longtitude)
            mapView.addAnnotation(annotation)
        }
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation{
            return nil
        }
        let pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        pin.canShowCallout = true
        pin.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        return pin
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let annView = view.annotation
        let storyBoard = UIStoryboard(name:"Main", bundle:nil)
        let carteleraTeatroVC = storyBoard.instantiateViewController(withIdentifier: "carteleraTeatro") as! carteleraTeatroViewController
        
        carteleraTeatroVC.nombre = ((annView?.title)!)!
        
        self.navigationController?.pushViewController(carteleraTeatroVC, animated: true)
    }
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 10000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}

