//
//  LoginViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/13/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import FirebaseAuth
import SystemConfiguration

class LoginViewController: UIViewControllerReach {

    
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkReachable()
        print(checkReachable2())

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //check if user is already signed
        if Auth.auth().currentUser != nil{
            
          //  user is already logged in
          //  let vc = self.storyboard?.instantiateViewController(identifier: "CarteleraVC")
           // self.present(vc!, animated: false, completion: nil)
            
        }

    }

  
    @IBAction func signInTapped(_ sender: Any) {
        
        let username = usernameTextField.text
        let password = passwordTextField.text
        
        Auth.auth().signIn(withEmail: username!, password: password!) { (user, error) in
            if error != nil{
                //error logging
                let errorMessage = error?.localizedDescription
                print(password!)
                print(username!)
                let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                //success
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainVC")
                if let navigator = self.navigationController{
                    self.navigationController?.isNavigationBarHidden = true;
                    //navigator.setViewControllers([vc!], animated:true)
                    navigator.present(vc!, animated: true, completion: nil)
                   // navigator.pushViewController(vc!, animated: true)
                }
                //self.present(vc!, animated: false, completion: nil)
            }
        
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class UIViewControllerReach: UIViewController {
    let reachability = SCNetworkReachabilityCreateWithName(nil, "www.google.com")
     func checkReachable(){
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(self.reachability!, &flags)
    
        if (!isNetworkReach(with:flags)){
            let alert = UIAlertController(title: "No hay conexión", message: "Es necesaria una conexión para poder acceder a estas funciones", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func checkReachable2()->Bool{
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(self.reachability!, &flags)
        if (!isNetworkReach(with:flags)){
            return false
        }
        else{
            return true;
        }
    }
    
     func isNetworkReach(with flags: SCNetworkReachabilityFlags)-> Bool{
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAuto = (flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic))
        
        let canConnectWithoutUserInteraction = canConnectAuto && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
}
