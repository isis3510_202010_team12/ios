//
//  ProfileViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/16/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    

    var imageFileName = ""
   // var obrasUsuario = [String]()
  

    var obras = NSMutableArray()

    @IBOutlet var previewImageView: UIImageView!
    @IBOutlet var selectImageBotton: UIButton!
    
   
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    var nombre = ""
    var fecha = ""
    var correo = ""
    var ratingNuevo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let _name = UserDefaults.standard.object(forKey: "name") as! String?
        let _date = UserDefaults.standard.object(forKey: "birthdate") as! String?
        let _username = UserDefaults.standard.object(forKey: "username") as! String?
        
        lblName.text = _name
        lblDate.text = _date
        lblUsername.text = _username
        
    
       let user = Auth.auth().currentUser

    if (user != nil){
        let reference = Database.database().reference().child("users").queryOrdered(byChild: "username")
        reference.queryEqual(toValue: user?.email).observeSingleEvent(of: .childAdded) { (snapshot) in
                                           
            let key = snapshot.key
           
            let valor = snapshot.value as! [String: Any ]

            let obras = valor["obras"] as? [String: Any]
            
            for obra in obras as! [String: Any]{
                
                let obraDetailValue = obra.value as! [String: String]
                let key2 = obraDetailValue.values.first as! String
                self.obras.add(key2)
           //     self.llaves.add(llaves)
                
                self.tableView.reloadData()

            }


          //  print(obrasUsuario)
         //   print(obrasUsuario.count)
            
            /*
            for obra in obrasUsuario{
                
                let title = obra[
                let reference = Database.database().reference().child("obras").queryOrdered(byChild: "title")
                      reference.queryEqual(toValue: user?.email).observeSingleEvent(of: .childAdded) { (snapshot) in
                
            }
             
            */
               // self.obras.add(obra)
            
        // Do any additional setup after loading the view.
    }

        }

       
        self.tableView.delegate = self
        self.tableView.dataSource = self
        


    }
    @IBAction func selectImageTapped(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    func uploadImage(image: UIImage){
        let randomName =  randomStringWithLength(length: 10)
        let imageData =  image.jpegData(compressionQuality: 1.0)
        let uploadRef = Storage.storage().reference().child("images/\(randomName).jpg")
        let uploadTask = uploadRef.putData(imageData!, metadata: nil){ metadata, error in
            if error == nil {
                //success
                print("Imagen cargada")
                self.imageFileName = "\(randomName as String).jpg"
            } else {
                //error
                print("Error cargando la imagen \(error?.localizedDescription)")
            }
        }
        
        
        
    }
    func randomStringWithLength(length: Int) -> NSString{
        let characters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString: NSMutableString = NSMutableString(capacity: length)
        for _ in 0..<length {
            let len = UInt32(characters.length)
            let rand = arc4random_uniform(len)
            randomString.appendFormat("%C", characters.character(at: Int(rand)))
        }
        return randomString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //will run the user hits cancel
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //will run the user finishes picking an image from photo library
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as?
        UIImage {
            self.previewImageView.image = pickedImage
            self.selectImageBotton.isEnabled = false
            self.selectImageBotton.isHidden = true
            uploadImage(image: pickedImage)
            picker.dismiss(animated: true, completion: nil)
        }
        
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           // #warning Incomplete implementation, return the number of rows
           return self.obras.count
       }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! ObrasUsuarioTableViewCell
   
        let reference =   Database.database().reference().child("obras").queryOrdered(byChild: "title")
        reference.queryEqual(toValue:obras[indexPath.row] ).observeSingleEvent(of: .value){ (snapshot) in
        let valor = snapshot.value as! [String: Any]
        let valores = valor.values
        let val = valores.first as! [String: Any]
        cell.titleLabelView.text = val["title"] as? String
        cell.teatroLabelView.text = val["theatre"] as? String
          //  cell.ratingLabelView.isHidden = true
            
            print(self.ratingNuevo)
           if self.ratingNuevo != "" {
            
            cell.calificarButton.setTitle("Ver", for: .normal)

              //  cell.ratingLabelView.text = self.ratingNuevo
                
            }
            
            
            
       // let num = val["rating"] as? Double
       // cell.ratingLabelView.text = "\(num!)"
   
        }
        
        /*
               let llave = self.obras[indexPath.row] as! String
               let obra = UserDefaults.standard.object(forKey: llave) as! Dictionary<String ,AnyObject>
               cell.descriptionLabelView.text = obra["description"] as? String
               cell.titleLabelView.text = obra["title"] as? String
               let num = obra["rating"] as? Double
               cell.ratingLabelView.text = "\(num!)"
               
               
                let imageData = obra["image"] as! Data
                let image = UIImage(data: imageData)
                cell.obraImageView.image = image
       */
        return cell
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
