//
//  RegisterViewController.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/14/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class RegisterViewController: UIViewControllerReach {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var inputTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    
    
    
    
    private var datePicker: UIDatePicker?
    
    override func viewDidLoad() {
        checkReachable()
        print(checkReachable2())
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(RegisterViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.viewTapped(gestureRecognizer:)))
        
        view.addGestureRecognizer(tapGesture)
        
        
        inputTextField.inputView=datePicker
        

        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped(gestureRecognizer:UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    
    
    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        inputTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        
        let username = usernameTextField.text
        let password = passwordTextField.text
        let birthdate = inputTextField.text
        let name = nameTextField.text
        
        Auth.auth().createUser(withEmail: username!, password: password!) { (user, error) in
            if error != nil{
                //error creating account
                
                let alert = UIAlertController(title: "Error", message: "Error creando la cuenta", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarteleraVC")
                
                if let navigator = self.navigationController{
                    self.navigationController?.isNavigationBarHidden = true;
                    //navigator.setViewControllers([vc!], animated:true)
                    //navigator.pushViewController(vc!, animated: true)
                    navigator.present(vc!, animated: true, completion: nil)
                }
                
                //self.present(vc!, animated: true, completion: nil)
            }
            
            
        }
        
        if let name = nameTextField.text{
            if let username = usernameTextField.text{
                if let birthdate = inputTextField.text{
                    let userObject: Dictionary<String,Any> = [
                        "name" : name,
                        "birthdate" : birthdate,
                        "username" : username
                    ]
                    Database.database().reference().child("users").childByAutoId().setValue(userObject)
                    print("User added")
                }
            }
        }
        
        /*let userObject = [
            
            "name" : name,
            "birthdate" : birthdate,
            "username" : username
        ]
        
        Database.database().reference().child("users").childByAutoId().setValue(userObject)
        
        print("Added new user. ")
 */
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
