//
//  ObrasUsuarioTableViewCell.swift
//  Prototype
//
//  Created by Susan Paola Joven Vasquez on 4/26/20.
//  Copyright © 2020 Luis Alfonso Ruiz. All rights reserved.
//

import UIKit

class ObrasUsuarioTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabelView: UILabel!
    
    
    @IBOutlet var teatroLabelView: UILabel!
    
    @IBOutlet var ratingLabelView: UILabel!
    
    @IBOutlet var calificarButton: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
